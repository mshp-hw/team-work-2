using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SM : MonoBehaviour
{
    Rigidbody2D rb;
    public bool scene = true;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        
    }

    // Update is called once per frame
    void Update()
    {
        
        if (gameObject.transform.position.x > 9f)
        {
            SceneManager.LoadScene("Scene1");
        }
        else if (scene == false)
        {
            Debug.Log("Winner");
        }
    }
}
