using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObj : MonoBehaviour
{
    public GameObject spawn;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        for (var i = 0; i < 10; i++)
        {
            Instantiate(spawn, new Vector3(i * 2.0f, 0, 0), Quaternion.identity);
        }
    }
}
