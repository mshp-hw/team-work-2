using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    float InHor;
    float InVert;
    Rigidbody2D rb;
    int speed = 5;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        InHor = Input.GetAxis("Horizontal");
        InVert = Input.GetAxis("Vertical");

        if (InHor != 0)
            rb.velocity = new Vector2(speed * InHor, rb.velocity.y);

        if (InHor > 0)
            gameObject.transform.localScale = new Vector3(1, 1, 1);

        if (InHor < 0)
            gameObject.transform.localScale = new Vector3(-1, 1, 1);
    }
}
